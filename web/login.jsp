<%--
    Licensed to the Apache Software Foundation (ASF) under one
    or more contributor license agreements.  See the NOTICE file
    distributed with this work for additional information
    regarding copyright ownership.  The ASF licenses this file
    to you under the Apache License, Version 2.0 (the
    "License"); you may not use this file except in compliance
    with the License.  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on an
    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, either express or implied.  See the License for the
    specific language governing permissions and limitations
    under the License.

--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%--
    This file is an entry point for JavaServer Faces application.
--%>
<f:view>
    <html xmlns:h="http://xmlns.jcp.org/jsf/html" xmlns:f="http://xmlns.jcp.org/jsf/core">
        <head>
            <link rel="stylesheet" href="bulma.css">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <title>LAB3 Homework JS</title>                     
        </head>
        <body>
            <div class="content">
           <h1><h:outputText value="Logowanie do aplikacji"/></h1>
            <h:form id="formularz_login">
            <h:outputText value="Nazwa użytkownika" /><br />
            <h:inputText value="#{logowanie.nazwa}" /><br /><br />

            <h:outputText value="Hasło" /><br />
            <h:inputSecret value="#{logowanie.hasło}" /><br />

            <h:commandButton value="Zaloguj" action="#{logowanie.sprawdz}" styleClass="button is-black"/>
            </h:form>
            </div>
        <footer class="footer">
            Praca wykonana przez Juliusza Swietonskiego na laboratoria <strong>Technologie JavaEE</strong>
        </footer>
        </body>
    </html>
</f:view>
