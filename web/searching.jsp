<%-- 
    Document   : search
    Created on : 2021-12-15, 21:42:42
    Author     : student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<!DOCTYPE html>
<f:view>
<html xmlns:h="http://xmlns.jcp.org/jsf/html" xmlns:f="http://xmlns.jcp.org/jsf/core">
    <head>
        <link rel="stylesheet" href="bulma.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>LAB3 Homework JS</title>
                <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
        <%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
    </head>
    <body>
        <div class="content">
           <h1><h:outputText value="Wyszukaj linie autobusu"/></h1>
            <h:form id="formularz_szukanie">
            <h:outputText value="Podaj numer lini ktora chcesz wyszukac" /><br />
            <h:inputText value="#{search.nazwa}" /><br /><br />
            <h:commandButton value="Wyszukaj" action="searchdb"  styleClass="button is-black"/>

            <h:commandButton value="Pokaz wszystkie rekordy" action="db"  styleClass="button is-black"/>
            </h:form>
            </div>
            <footer class="footer">
            Praca wykonana przez Juliusza Swietonskiego na laboratoria <strong>Technologie JavaEE</strong>
        </footer>
    </body>
</html>
</f:view>