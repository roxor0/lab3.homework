<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<f:view>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>LAB3 Homework JS</title>
         <link rel="stylesheet" href="bulma.css">
        <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
        <%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
    </head>
    <body>
       
       <c:import url="WEB-INF/juliusz_base.xml" var="baza" />
        <x:parse doc="${baza}" var="wynik" />
 
        <div class="content">       
            <c:set var = "line" scope = "session" value = "${search.nazwa}"/>
             
            <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
    <thead>
    <tr>
      <th>Carrier</th>
      <th>Line Number</th>
      <th>Route</th>
      <th>Travel start</th>
      <th>Travel end</th>
      <th>Passes through</th>
      <th>Number of places</th>
      <th>Ticket price</th>
      <th>Has place for children</th>
      <th>Has buffet</th>
    </tr>
  </thead>
  <x:forEach select="$wynik/buslines/bus[number = $sessionScope:line]" var="zasob">
      
      <tr>
     <td><x:out select="carrier" /></td>
     <td><x:out select="number" /></td>
     <td><x:out select="route" /></td>
     <td><x:out select="travel_start" /></td>
     <td><x:out select="travel_end" /></td>
     <td><x:out select="passes_through" /></td>
     <td><x:out select="Number_of_places" /></td>
     <td><x:out select="ticket_price" /></td>
     <td><x:out select="has_place_for_children" /></td>
     <td><x:out select="has_buffet" /></td>
 </tr>
    
</x:forEach>
</table>   
</ol>
        <h:form id="form_logout">
            <h:outputText value="Kliknij aby sie wylogowac" /><br />     
                <h:commandButton value="Wyloguj" action="logout" styleClass="button is-black" />           
            </h:form>
             </div>
            <footer class="footer">
            Praca wykonana przez Juliusza Swietonskiego na laboratoria <strong>Technologie JavaEE</strong>
        </footer>
    </body>
</html>
</f:view>

