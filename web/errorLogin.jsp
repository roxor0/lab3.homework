<%-- 
    Document   : errorLogin
    Created on : 2021-12-10, 11:53:04
    Author     : student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<!DOCTYPE html>
 <f:view>
<html>
    <head>
        <link rel="stylesheet" href="bulma.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>LAB3 Homework JS</title>
    </head>
    <body>
        <div class="content">
            <h1><h:outputText value="Błąd logowania"/></h1>
            <h:form id="formularz_error">
            <h:outputText value="Podana nazwa użytkownika: #{logowanie.nazwa}
            jest nieprawidłowa, lub podano błędne dla niej hasło! " /><br />
            <h:commandButton value="Spróbuj ponownie" action="retry" styleClass="button is-black"/>
       
            </h:form>
        </div>
            <footer class="footer">
            Praca wykonana przez Juliusza Swietonskiego na laboratoria <strong>Technologie JavaEE</strong>
        </footer>
    </body>
 </html>
</f:view>
