<%-- 
    Document   : logout
    Created on : 2021-12-10, 12:45:33
    Author     : student
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<!DOCTYPE html>
 <f:view>
<html>
    <head>
        <link rel="stylesheet" href="bulma.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>JSF error page</title>
    </head>
    <body>
        <div class="content">
            <h1><h:outputText value="Wylogowano"/></h1> 
        </div>
    </body>
 </html>
</f:view>
